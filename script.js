(function() {
    const alarmCheckButton = document.getElementById("alarmCheckButton");
    const alarmStopButton = document.getElementById("alarmStopButton");

    const requestIntervalMinutesInput = document.getElementById(
        "requestIntervalMinutesInput"
    );
    const startCheckingButton = document.getElementById("startCheckingButton");
    const stopCheckingButton = document.getElementById("stopCheckingButton");

    const audioFile = new Audio("./alarm.mp3");

    let checkingInterval = null;
    requestIntervalMinutesInput.value = 1;

    alarmCheckButton.addEventListener("click", soundTheAlarm);
    alarmStopButton.addEventListener("click", stopTheAlarm);
    startCheckingButton.addEventListener("click", startChecking);
    stopCheckingButton.addEventListener("click", stopChecking);

    function startChecking() {
        console.log("CHECK STARTED");
        console.log(
            `REFRESH EVERY: ${requestIntervalMinutesInput.value} MINUTES`
        );

        checkForSlots();
        checkingInterval = setInterval(
            () => checkForSlots(),
            requestIntervalMinutesInput.value * 60 * 1000
        );
    }

    function stopChecking() {
        console.log("CHECK STOPPED");
        clearInterval(checkingInterval);
    }

    function checkForSlots() {
        getSlots().then(result => {
            if (hasFreeSlots(result)) {
                soundTheAlarm();
            }
        });
    }

    function hasFreeSlots(slots) {
        return slots.some(x => x.available || x.remaining > 0);
    }

    function soundTheAlarm() {
        audioFile.play();
    }

    function stopTheAlarm() {
        audioFile.pause();
    }

    function getSlots() {
        let today = new Date();

        return httpGet(
            `https://www.ebag.bg/orders/get-time-slots?shipping_date=${today.getFullYear()}-${today.getMonth() +
                1}-${today.getDate() + 1}`
        ).then(result => {
            console.log("SLOTS LOADED", result.data.time_slots);

            return result.data.time_slots;
        });
    }

    function httpGet(url) {
        url = "https://cors-anywhere.herokuapp.com/" + url;

        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();

            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            };

            xhr.open("GET", url);
            xhr.send(null);
        });
    }
})();
